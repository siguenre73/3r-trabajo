using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health = 3;  // Vida del enemigo
    public float speed = 3f; // Velocidad del enemigo

    private Transform player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        // Rotar hacia el jugador
        Vector2 rotateDirection = player.position - transform.position;
        float angle = Mathf.Atan2(rotateDirection.y, rotateDirection.x) * Mathf.Rad2Deg - 90f;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);

        // Mover hacia el jugador
        transform.position += transform.up * speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // Si el enemigo colisiona con el jugador, quitar una unidad de vida
            collision.gameObject.GetComponent<Player>().TakeDamage(1);

            // Si el enemigo no est� muerto, reducir su vida
            if (health > 0)
            {
                health--;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}

